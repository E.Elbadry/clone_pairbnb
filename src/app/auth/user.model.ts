export class User {
    constructor(public id: string, public email: string,  private _token:string, private _tokenExperationDate: Date) {}

    get token() {
        if(!this._token || this._tokenExperationDate > new Date()) {
            return this._token;
        } else {
            return null;
        }
    }
}