import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { User } from './user.model';
import { map, tap } from 'rxjs/operators';
export interface AuthDateResponse {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  _user = new BehaviorSubject<User>(null);
   private _isAuthenctedStatus = true;
  private _userID = 'abc';
  constructor(
    private http: HttpClient
  ) { }

    get userID() {
    return this._userID;
  }

  get isAuthenticatedStatus() {
    return this._isAuthenctedStatus;
  }

  login() {
    this._isAuthenctedStatus = true;
  }

  logout() {
    this._isAuthenctedStatus = false;
  }

  // get userID() {
  //   return this._user.asObservable().pipe(map(user => {
  //     if(user) {
  //       return user.id;
  //     } else {
  //       return null;
  //     }
  //   }))
  // }

  // get isAuthenticatedStatus() {
  //   return this._user.asObservable().pipe(map(user => { 
  //     if(user) {
  //       return !!user.token
  //     } else {
  //       return false;
  //     }
  //   }));
    
  // }

  // signup(email: string, password: string) {
  //   console.log(environment.firebaseApiKey);
    
  //   return this.http.post<AuthDateResponse>(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.firebaseApiKey}`, 
  //   {
  //     email: email,
  //     password: password,
  //     returnSecureToken: true
  //   }
  //   ).pipe(tap(this.setUserData.bind(this)));
  // }

  // signin(email:string, password: string) {
  //   return this.http.post<AuthDateResponse>(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.firebaseApiKey}`, {
  //     email: email,
  //     password: password
  //   }).pipe(tap(this.setUserData.bind(this)));
  // }

  // private setUserData(userData: AuthDateResponse) {
  //   const expirtationTime = new Date( new Date().getTime() + (+userData.expiresIn * 1000) );

  //   this._user.next(new User(userData.localId, userData.email, userData.idToken, expirtationTime));
  // }

  // logout() {
  //   this._user.next(null);
  // }
}
