import { Component, OnInit, ɵCodegenComponentFactoryResolver } from '@angular/core';
import { AuthService, AuthDateResponse } from './auth.service';
import { Router } from '@angular/router';
import {LoadingController, AlertController} from '@ionic/angular';
import {NgForm} from '@angular/forms';
import { Observable } from 'rxjs';
@Component({
  selector: "app-auth",
  templateUrl: "./auth.page.html",
  styleUrls: ["./auth.page.scss"]
})
export class AuthPage implements OnInit {
  isLoggin = true;
  constructor(
    private auth: AuthService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {}

  // authenticate(email: string, password: string) {
    
  //   this.loadingCtrl
  //     .create({ keyboardClose: true, message: "Logging In..." })
  //     .then(loadingEl => {
  //       loadingEl.present();

  //         let authObs: Observable<AuthDateResponse>;
  //         if(this.isLoggin) {
  //           authObs = this.auth.signin(email, password);
  //         } else {
  //           authObs = this.auth.signup(email, password);
  //         }

  //         authObs.subscribe(res => {
  //           console.log("close");
  //           loadingEl.dismiss();
  //           this.router.navigateByUrl("/places/tabs/discover");
  //         }, (errResponse) => {
  //           loadingEl.dismiss();
  //           let message = 'Could Not Sign you up, please try again!';
  //           const error = errResponse.error.error.errors[0].message;
  //           if(error === 'EMAIL_EXISTS') {
  //             message = 'this is account is exists already!';
  //           } else if(error === 'EMAIL_NOT_FOUND'){
  //             message = 'your email could not found !';
  //           } else if(error === 'INVALID_PASSWORD') {
  //             message = 'your password not correct .. please try again!';
  //           } 

  //           this.alertMessage(message);
  //         }
  //         );
     
  //     });
  // }

  // alertMessage(message: string) {
  //   this.alertCtrl.create({header: 'Authenticate Faild', message: message, buttons: ['Okay']}).then((loadingEl) => loadingEl.present());
  // }

  // onChangeMode() {
  //   this.isLoggin = !this.isLoggin;
  // }

  // onSubmit(form: NgForm) {
  //   console.log(form);
  //   if(!form.valid) {
  //     return ;
  //   }

  //   const email = form.value.email;
  //   const password = form.value.password;

  //   if(!this.isLoggin) {
  //     this.authenticate(email, password);
     
  //   } else {
  //     this.authenticate(email, password);
      
     
  //   }

  // }



   onLogin() {
    this.auth.login();
    this.loadingCtrl
      .create({ keyboardClose: true, message: "Logging In..." })
      .then(loadingEl => {
        loadingEl.present();

        setTimeout(() => {
          console.log("close");
          loadingEl.dismiss();
          this.router.navigateByUrl("/places/tabs/discover");
        }, 1000);
      });
  }

  onChangeMode() {
    this.isLoggin = !this.isLoggin;
  }

  onSubmit(form: NgForm) {
    console.log(form);
    if(!form.valid) {
      return ;
    }

    this.onLogin();
  }
}
